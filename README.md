# anomaly_detection
To run this script, run the following commands:
```
- docker-compose up -d
- sudo cp -rf  ../spark_script docker/data/
- docker exec -it spark-master bash
- cd spark/bin
- ./spark-submit --master spark://spark-master:7077 --total-executor-cores 1 --executor-memory 1g /tmp/data/spark_script/anomaly_detector.py

