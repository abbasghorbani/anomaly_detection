<h1 id="title" align="center">Generator</h1>
Generator is a golang library for generates real-time data samples.

<h2>-brokers:</h2>
This flag use to define kafka bootstrap brokers.default value is "kafka0:9092,kafka1:9093, kafka2:9094"

<h2>Usage:</h2>
Before run this command you must set KAFKA_URL in os environment

```bash
go run main.go -brokers="kafka0:9092,kafka1:9093,kafka2:9094"
```