package kafka

import (
	"time"

	"github.com/Shopify/sarama"
	log "github.com/sirupsen/logrus"
)

type Producer struct {
	SyncProducer  sarama.SyncProducer
	AsyncProducer sarama.AsyncProducer
}

func NewProducer(brokerList []string) *Producer {

	producer := &Producer{
		// SyncProducer: GetSyncProducer(brokerList),
		AsyncProducer: GetAsyncProducer(brokerList),
	}
	return producer
}

func GetAsyncProducer(brokerList []string) sarama.AsyncProducer {

	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForLocal       // Waits for only the local commit to succeed before responding.
	config.Producer.Compression = sarama.CompressionLZ4      // Compress messages
	config.Producer.Flush.Frequency = 500 * time.Millisecond // Flush batches every 500ms

	producer, err := sarama.NewAsyncProducer(brokerList, config)
	if err != nil {
		log.Fatalln("Failed to start Sarama producer:", err)
	}

	go func() {
		for err := range producer.Errors() {
			log.Println("Failed to write access log entry:", err)
		}
	}()

	return producer
}

func GetSyncProducer(brokerList []string) sarama.SyncProducer {

	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.NoResponse
	config.Producer.Retry.Max = 5 // Retry up to 5 times to produce the message
	config.Producer.Return.Successes = true
	config.Producer.Return.Errors = true
	config.Producer.Flush.Frequency = 500 * time.Millisecond
	config.Producer.Compression = sarama.CompressionSnappy

	producer, err := sarama.NewSyncProducer(brokerList, config)
	if err != nil {
		log.Fatalln("Failed to start Sarama producer:", err)
	}

	return producer
}

func (producer *Producer) ProduceMessageAsync(topic string, msg []byte, key string) {
	message := &sarama.ProducerMessage{
		Topic: topic,
		Value: sarama.StringEncoder(msg),
		Key:   sarama.StringEncoder(key),
	}
	producer.AsyncProducer.Input() <- message
}

func (producer *Producer) ProduceMessageSync(topic string, msg []byte, key string) {
	message := &sarama.ProducerMessage{
		Topic: topic,
		Value: sarama.StringEncoder(msg),
		Key:   sarama.StringEncoder(key),
	}
	// partition, offset, err := producer.SyncProducer.SendMessage(message)
	_, _, err := producer.SyncProducer.SendMessage(message)

	if err != nil {
		log.Printf("Failed to store your data:, %s", err)
	}
}

func (producer *Producer) AyncProducerClose() {
	if err := producer.AsyncProducer.Close(); err != nil {
		log.Fatalln(err)
	}
}

func (producer *Producer) SyncProducerClose() {
	if err := producer.SyncProducer.Close(); err != nil {
		log.Fatalln(err)
	}
}
