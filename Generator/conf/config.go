package conf

import (
	"path/filepath"
	"runtime"
	"sync"

	log "github.com/sirupsen/logrus"
)

var loadConfigOnce sync.Once

type Config struct {
	SAMPLE_FILE_PATH string
}

var config Config

func Conf() Config {
	loadConfigOnce.Do(loadConfig)
	log.Info("loaded config...")
	return config
}

func loadConfig() {

	_, b, _, _ := runtime.Caller(0)
	basepath := filepath.Dir(b)
	config.SAMPLE_FILE_PATH = filepath.Join(basepath, "../docs/cpu_usage.data")

}
