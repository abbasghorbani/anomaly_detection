package main

import (
	"Generator/conf"
	"Generator/utils"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"strings"
	"syscall"

	log "github.com/sirupsen/logrus"
)

func init() {

	log.SetFormatter(&log.TextFormatter{FullTimestamp: true, PadLevelText: true})
	// log.SetReportCaller(true)
	log.SetLevel(log.DebugLevel)

}

func main() {
	kafkaBootstrap := flag.String("brokers", "kafka0:9092,kafka1:9093,kafka2:9094", "# Define kafka bootstrap brokers to connect to, as a comma separated list")

	flag.Parse()

	keepRunning := true
	done := make(chan bool)

	// initial config
	config := conf.Conf()

	sampleData, err := utils.ReadSampleFile(config.SAMPLE_FILE_PATH)
	if err != nil {
		log.Error(err)
		return
	}

	// Generate sample data
	brokers := strings.Split(fmt.Sprint(*kafkaBootstrap), ",")

	go utils.StateDataGenerator(brokers, sampleData, done)

	// listen to signal
	sigterm := make(chan os.Signal, 1)
	signal.Notify(sigterm, syscall.SIGINT, syscall.SIGTERM)

	for keepRunning {
		select {
		case <-done:
			log.Info("The generator has been successfully completed")
			keepRunning = false
		case <-sigterm:
			log.Info("Process terminates with user")
			keepRunning = false
		}
	}
}
