package utils

import (
	"Generator/services/kafka"
	"bufio"
	"encoding/json"
	"os"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
)

type CPUState struct {
	Timestamp int64   `json:"timestamp"`
	CPU       float64 `json:"cpu"`
}

func NewCPUState() *CPUState {
	return &CPUState{}
}

func ReadSampleFile(filename string) ([]string, error) {

	var fileLines []string

	readFile, err := os.Open(filename)

	if err != nil {
		return fileLines, err
	}

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	// Ignore first line
	fileScanner.Scan()

	for fileScanner.Scan() {
		fileLines = append(fileLines, fileScanner.Text())
	}

	readFile.Close()

	log.Info("The sample data file load successfully...")

	return fileLines, nil
}

func StateDataGenerator(brokers []string, sampleData []string, done chan bool) {

	// Create kafka producer
	producer := kafka.NewProducer(brokers)

	for _, line := range sampleData {
		state := NewCPUState()
		item := strings.Split(line, ",")
		state.Timestamp, _ = strconv.ParseInt(item[0], 10, 64)
		state.CPU, _ = strconv.ParseFloat(item[1], 64)
		key := "sensor_1"

		data, err := json.Marshal(state)
		if err != nil {
			log.Error(err)
			done <- true
		}
		producer.ProduceMessageAsync("state", data, key)
	}
	log.Info("All data sent to Kafka")

	err := producer.AsyncProducer.Close()
	if err != nil {
		log.Error(err.Error())
	}

	done <- true
}
