import os

from pyspark.sql import functions as F
from pyspark.sql.session import SparkSession
from pyspark.sql.types import (LongType, FloatType, StructField, StructType)
from statistics import mean


####### Parameters ##############

bootstrapServers = "kafka0:9092,kafka1:9093,kafka2:9094"
topics = "state"
window_duration = "2 seconds"

schema = StructType([
    StructField("timestamp", LongType(), False),
    StructField("cpu", FloatType(), True),
])


############## Define local function ##################


def create_spark_session():
    """ Create spark session"""
    spark = SparkSession.builder.appName("anomaly_detector")\
        .config("spark.streaming.stopGracefullyOnShutdown", "true")\
        .config("spark.eventLog.enabled", "true")\
        .config("spark.serializer", "org.apache.spark.serializer.KryoSerializer")\
        .config("spark.streaming.backpressure.enabled", "true")\
        .config("spark.sql.streaming.metricsEnabled", "true")\
        .config("spark.default.parallelism", "16")\
        .config("spark.streaming.kafka.maxRatePerPartition", "10000")\
        .config("spark.streaming.receiver.maxRate", "-1")\
        .config("spark.streaming.backpressure.initialRat", "1000")\
        .config("spark.streaming.unpersist", "true")\
        .config("spark.streaming.receiver.writeAheadLog.enable", "true")\
        .config("spark.files.useFetchCache", "true")\
        .config('spark.jars.packages', 'org.apache.spark:spark-streaming-kafka-0-10_2.12:3.2.1')\
        .config('spark.jars.packages', 'org.apache.spark:spark-sql-kafka-0-10_2.12:3.2.1')\
        .getOrCreate()

    spark.sparkContext.setLogLevel("WARN")

    return spark


def detect_anomaly(data_frame):

    windows = data_frame\
        .groupBy(F.window("unix_timestamp", "1 seconds").alias("window"))

    aggregatedDF = windows\
        .agg(F.mean("cpu").alias("cpu_average"), F.variance("cpu").alias("cpu_variance"))\
        .select("window",
                "cpu_average",
                "cpu_variance")

    return aggregatedDF


def subscribe_from_kafka(spark_session):
    """ Streaming data from Kafka topic as a dataFrame and convert data to json struct """
    data_frame = spark_session\
        .readStream\
        .format("kafka")\
        .option("kafka.bootstrap.servers", bootstrapServers)\
        .option("subscribe", topics)\
        .option("heartbeat.interval.ms", "3000")\
        .option("max.poll.interval.ms", "300000")\
        .option("request.timeout.ms", "305000")\
        .option("connections.max.idle.ms", "540000")\
        .option("fetch.max.wait.ms", "500")\
        .option("fetch.min.bytes", "100000")\
        .option("fetch.max.bytes", "52428800")\
        .option("startingOffsets", "latest")\
        .load()\
        .select(F.from_json(F.col("value").cast("string"), schema).alias("parsed_value"))\
        .select("parsed_value.*").dropDuplicates(['timestamp'])

    return data_frame


def write_result(data_frame):
    query = data_frame\
        .writeStream\
        .format("complete")\
        .outputMode("update")\
        .option("truncate", "false")\
        .start()

    return query


############## Run spark job ##################
if __name__ == "__main__":

    spark = create_spark_session()
    data_frame = subscribe_from_kafka(spark)

    df_with_unix_timestamp = data_frame\
        .withColumn("unix_timestamp", F.to_timestamp(F.col("timestamp")/1000))

    result = detect_anomaly(df_with_unix_timestamp)
    query = write_result(result)
    query.awaitTermination()
